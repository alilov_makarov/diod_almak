﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using A1i1uyamax.KA;

namespace Diod_ALMAK
{
    public partial class Form1 : Form
    {

        Pen pen_main; // ручка для катодов
        Brush B_points; //кисть для точек
        Bitmap bmp;

        const double q = -1.6E-19;
        const double m = 9.1E-31;

        const double K = 8.99E9; // постоянная "к" в напряженности ( 1/4 Pi E0)

        Graphics g;
        Random rand;

        double dt;// шаг по времени

        int N; // количество точек

        Size sz;

        double Ua, Uk;
        KAVector3D E;

        double Q, t, dU;

        List<Particle> points; // лист, хранящий координаты и скорости частиц

        public Form1()
        {
            InitializeComponent();
            sz = picture.Size;
            N = 50;
            dt = 0.01;
            rand = new Random(DateTime.Now.Millisecond);
        }

        private void button_run_Click(object sender, EventArgs e)
        {
            pen_main = new Pen(Color.Black, 3);
            B_points = Brushes.Red;

            Ua = 0;
            Uk = Convert.ToDouble(textBox2.Text);
            dU = Convert.ToDouble(textBox1.Text);
            E = new KAVector3D(-(Uk - Ua) / sz.Width, 0, 0);
            t = 0;
            Q = 0;

            points = new List<Particle>();//инициализация листа частиц
            Start();
        }

        /// <summary>
        /// Отрисовать точку.
        /// </summary>
        /// <param name="g">Гэ</param>
        /// <param name="p">Точка</param>
        /// <remarks>
        /// Ну а чё, каждый раз прописывать? Лениво)
        /// </remarks>
        public void DrawPoint(Graphics g, Particle p)
        {
            float b = 4.5f;
            g.FillEllipse(B_points, (float)p.r.X - b / 2, (float)p.r.Y - b / 2, b, b);
        }

        /// <summary>
        /// функция начальной отрисовки
        /// </summary>
        void Start()
        {
            bmp = new Bitmap(sz.Width, sz.Height);
            g = Graphics.FromImage(bmp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            //g.SmoothingMode = SmoothingMode.HighSpeed;
            g.Clear(Color.White);
            picture.Image = bmp;
            //chart1.Series[0].Points.Clear();
            button_pause.Enabled = true;
            timer.Enabled = true;
        }

        /// <summary>
        /// генерация точек у катода
        /// </summary>
        void generatePoints()
        {

            for (int i = 0; i < N; i++)
            {
                var p = new Particle(2 + rand.NextDouble(), rand.Next(2, sz.Height - 5), rand.NextDouble(), 2 * rand.NextDouble() - 1);

                points.Add(p); // добавляем сгенерированные точки в лист

                DrawPoint(g, p);
            }
        }

        void CalcCoord()
        {
            KAVector3D f;
            var F = new KAVector3D[points.Count];

            for (int i = 0; i < points.Count(); i++)
            {
                f = new KAVector3D(); // сумма c E
                for (int j = 0; j < points.Count(); j++)
                {
                    if (j != i)
                    {
                        var r = new KAVector3D(points[i].r.X - points[j].r.X,
                            points[i].r.Y - points[j].r.Y, 0);
                        f += r / (r * r) / r.Module();
                    }
                }
                F[i] = K * q * q * f + q * E;
            }
            // пересчёт скоростей и координат методом Эйлера.
            // Грубо, конечно, но это самый быстрый метод, а у нас и так всё лагает.
            for (int i = 0; i < points.Count(); i++)
            {
                points[i].v += F[i] * dt / m;
                points[i].r += points[i].v * dt;
                t += dt;
                // Если западает в аноде или катоде, то делитим частичку.
                if (points[i].r.X > sz.Width)
                {
                    Q += Math.Abs(q);
                    points.RemoveAt(i--);
                }
                else if ((points[i].r.X < 0) || (points[i].r.Y < 0) ||
                    (points[i].r.Y > sz.Height)) points.RemoveAt(i--);
                //else if (points[i].r.Y > sz.Height) points[i].r.Y -= sz.Height;
                //else if (points[i].r.Y < 0) points[i].r.Y += sz.Height;
            }
        }
        int M = 0;
        double I = 0;
        private void timer_Tick(object sender, EventArgs e)
        {
            g.Clear(Color.White);
            generatePoints();//генерим точки на катоде
            CalcCoord();

            foreach (var p in points)
            {
                DrawPoint(g, p);
            }
            picture.Image = bmp;

            if (t > 10)
            {
                I += Q / t;
                Q = 0;
                t = 0;
                M++;
            }
            if (M > 10)
            {
                chart1.Series[0].Points.AddXY(Uk - Ua, I / M);
                M = 0;
                Uk += dU;
                I = 0;
                E = new KAVector3D(-(Uk - Ua) / 2E+8, 0, 0);
                textBox2.Text = Uk.ToString();
            }
        }

        
        private void button_pause_Click(object sender, EventArgs e)
        {
            timer.Enabled ^= true;
        }


    }
}
