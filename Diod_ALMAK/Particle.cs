﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using A1i1uyamax.KA;

namespace Diod_ALMAK
{
    /// <summary>
    /// Использовать будем только X и Y компоненты, что очевидно.
    /// Библиотечка включает в себя xml-документацию, так что
    /// разбираться в ней даже не надо: всё будет написано, если
    /// навести курсор. Пусть она не пугает. Рабочая на 100%.
    /// </summary>
    public class Particle
    {
        public KAVector3D r; // координата
        public KAVector3D v; // скорость

        public Particle(double x, double y, double Vx, double Vy)
        {
            r = new KAVector3D(x, y, 0);
            v = new KAVector3D(Vx, Vy, 0);
        }
    }
}